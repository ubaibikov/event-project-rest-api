<?php

use App\User;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Participant;

class ParticipantTest extends TestCase
{
    protected $testId = 12345;
    public function testParticipantCreate_CreateWihotRequestValues_Failed()
    {
        $this->post('/api/events/1/create-participants?token=' . env('API_TESTS'));
        $this->seeStatusCode(206);
    }

    public function testParticipantsCreate_CreateWithRequestValues_Success()
    {
        $this->post('/api/events/1/create-participants?token=' . env('API_TESTS'), [
            'name' => 'Test',
            'last_name' => 'Test LastName',
            'email' => 'CreateWithRequestValues@mail',
        ]);

        $this->seeStatusCode(200);
    }

    public function testParticipantsCreate_CreateWithCreatedEmail_Failed()
    {
        $this->post('/api/events/1/create-participants?token=' . env('API_TESTS'), [
            'name' => 'Test 2',
            'last_name' => 'Test LastName 2',
            'email' => 'CreateWithRequestValues@mail',
        ]);
        $this->seeStatusCode(206);
    }

    public function testParticipantsGet_WithoutParticipiants_()
    {
        $participants = Participant::where('event_id', 3)->delete();

        $this->get('/api/get/events/3/participants?token=' . env('API_TESTS'));

        $this->seeStatusCode(400);
    }

    public function testParticipantsGet_WithParticipiants_Success()
    {
        $this->get('/api/get/events/1/participants?token=' . env('API_TESTS'));
        $this->seeStatusCode(200);
    }

    public function testParticipantGetCurrentParticipiant_WithParticipiantFieldId_Success()
    {
        $participantFind = Participant::where('event_id', 1)->select('id')->get()->first();
        $this->get('/api/get/events/1/participants/' . $participantFind->id . '?token=' . env('API_TESTS'));
        $this->seeStatusCode(200);
    }

    public function testParticipantGetCurrentParticipiant_WithoutParticipiantFieldId_Failed()
    {
        $this->get('/api/get/events/1/participants/' . $this->testId . '?token=' . env('API_TESTS'));
        $this->seeStatusCode(404);
    }

    public function testParticipantUpdateCurrentParticipant_Success()
    {
        $participantId = Participant::where('email' , 'CreateWithRequestValues@mail')->select('id')->get()->first();

        $this->put('/api/events/1/participants/update/' . $participantId->id . '?token=' . env('API_TESTS'), [
            'name' => 'TEstUpdate',
            'last_name' => 'TESTLASTNAME UPDATE',
            'email' => 'TEST@EMAILUPDATE',
        ]);
        $this->seeStatusCode(200);

        $participantId->delete();
    }
}
