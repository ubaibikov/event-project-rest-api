<?php

use Illuminate\Database\Seeder;
use App\Event;
use Carbon\Carbon;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Event::create([
            'name' => 'Ulcamp',
            'date' => Carbon::now(),
            'city' => 'Ulyanovsk',
        ]);

        Event::create([
            'name' => 'ROCK CATHARSIS',
            'date' => Carbon::now(),
            'city' => 'Vladivastok',
        ]);

        Event::create([
            'name' => 'Drink Time',
            'date' => Carbon::now(),
            'city' => 'Moscow',
        ]);
    }
}
