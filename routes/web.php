<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\User;

$router->get('/', function () use ($router) {

    return $router->app->version();
});

$router->post('/api/auth/login', ['uses' => 'AuthController@authenticate']);

$router->group(['prefix' => 'api', 'middleware' => 'jwt.auth'], function () use ($router) {
    $router->get('get-events', 'EventController@index');
    $router->get('get-events/{id}', 'EventController@event');
    $router->post('events/{eventId}/create-participants','ParticipantController@create');
    $router->get('get/events/{eventId}/participants','ParticipantController@getParticipants');
    $router->get('get/events/{eventId}/participants/{participantId}','ParticipantController@getCurrentParticipant');
    $router->put('events/{eventId}/participants/update/{participantId}','ParticipantController@updateCurrentParticipant');
    $router->delete('events/{eventId}/participants/delete/{participantId}','ParticipantController@deleteCurrentParticipant');
});
