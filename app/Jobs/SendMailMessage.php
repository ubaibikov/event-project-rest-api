<?php

namespace App\Jobs;

class SendMailMessage extends Job
{
    protected $mailMessage;
    protected $email;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $email, string $message)
    {
        $this->mailMessage = $message;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        info("$this->email : $this->mailMessage");
    }
}
