<?php

namespace App\Http\Controllers;

use App\Event;

class EventController extends Controller
{
    public function index()
    {
        $events = app('db')->select("SELECT id, name, date, city FROM events");
        if ($events) {
            return response()->json($events, 200);
        }

        return response()->json([
            'status' => '404',
            'message' => 'Events not found',
        ], 404);
    }

    public function event($id)
    {
        $event = Event::select('id', 'name', 'date', 'city')
            ->where('id', $id)
            ->get()
            ->first();

        if ($event) {
            return response()->json($event, 200);
        }

        return response()->json([
            'status' => 404,
            'message' => 'Event Not Found',
        ], 404);
    }
}
