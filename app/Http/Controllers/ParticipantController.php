<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Participant;
use App\Jobs\SendMailMessage;
use Illuminate\Support\Carbon;

class ParticipantController extends Controller
{
    public function create(int $eventId, Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:participants'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
                'status' => 206
            ], 206);
        }

        $createParticipant = Participant::create([
            'name' => $request->name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'event_id' => $eventId,
        ]);

        if ($createParticipant) {
            $this->sendParticipantEmailMessage($request->email);
            return response()->json([
                'status' => 200,
                'message' => 'Participant is created'
            ], 200);
        }
    }

    public function sendParticipantEmailMessage(string $mail)
    {
        $job = (new SendMailMessage($mail, "Participant Create"))->delay(Carbon::now()->addMinutes(5));
        $this->dispatch($job);
    }

    public function getParticipants(int $eventId)
    {
        $participants = Participant::where('event_id', $eventId)
            ->select('id', 'name', 'last_name', 'email')
            ->get();

        if (count($participants)> 1) {
            return response()->json([
                'status' => 200,
                'participants' => $participants,
            ]);
        }

        return response()->json([
            'status' => 400,
            'message' => ' In this Event not Participants',
        ], 400);
    }

    public function getCurrentParticipant($eventId,  $participantId)
    {
        $participant = Participant::select('id', 'name', 'last_name', 'email', 'event_id')
            ->where([
                'id' =>  $participantId,
            ])->get()->first();

        if (!$participant) {
            return response()->json([
                'status' => 404,
                'message' => "Participant is not defined",
            ], 404);
        }

        return response()->json([
            'status' => 200,
            'participant' => $participant,
        ], 200);
    }

    public function updateCurrentParticipant($eventId, $participantId, Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
                'status' => 206,
            ], 206);
        }

        $participant = Participant::where('id', $participantId)
            ->update([
                'name' => $request->name,
                'last_name'=> $request->last_name,
                'email' => $request->email,
            ]);

        return response()->json([
            'status' => 200,
            'message' => 'Participant is updated'
        ], 200);
    }

    public function deleteCurrentParticipant($eventId, $participantId, Request $request)
    {
        $participantDelete = Participant::find($participantId);

        if ($participantDelete) {
            $participantDelete->delete();
        }

        if ($participantDelete->id) {
            return response()->json([
                'status' => 200,
                'message' => 'Participant is deleted'
            ], 200);
        }

        return response()->json([
            'status' => 404,
            'message' => 'Participant is not defined'
        ], 404);
    }
}
