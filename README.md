# Event Project

## Setup Project

###### composer install

###### rename env.example - .env

###### php artisan migrate
 
###### php artisan db:seed

###### php artisan queue:work --tries=3 

## Rest Api Docs

После установки проекта , в таблице users появятся пользоватли 
чтобы начать пользоваться API вы должны получить token ,
вы можете получить токен сделав post запрос по url /api/auth/login
Введите любой email указанный в таблице users а также password : 12345

Напрмер: 
###### curl -X POST -F 'email=table_users_email' -F 'password=12345' /api/auth/login

response:

###### token : eyJ0eXAiOiJKV1QiLCJh.....


#### Работа с API
###### curl GET /api/get-events?token=полученный_токен  - возвращает все мероприятия 
###### curl GET /api/get-events/ID?token=полученный_токен  - Возвращает конктретное мероприятия по id 
###### curl -X POST -F 'email=test@email' -F 'name=testName' -F 'last_name=testLastName' /api/events/ID/create-participants?token=полученный_токен - Создает участника с привязкой к мероприятию
###### curl GET /api/get/events/ID/participants?token=полученный_токен - Возвращает всех участинков конкретного мероприятия
###### curl GET /api/get/events/ID/participants/PARTICIPANTID?token=полученный_токен - возвращает конкртеного пользователя мероприятия
###### curl -X PUT -F 'email=test@updateEmail' -F 'name=name' -F 'last_name=last_name' /api/events/ID/participants/update/PARTICIPANTID?token=полученный_токен - обновляет участника мероприятия 
###### curl DELETE /api/events/ID/participants/delete/PARTICIPANTID?token=полученный_токен - удаляет участиника мероприятия 

## Тестирование 

###### В файле .env в поле API_TESTS - добавьте полученный вами токен. 
